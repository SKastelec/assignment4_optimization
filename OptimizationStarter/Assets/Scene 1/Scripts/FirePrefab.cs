﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    // public reference to gameobject Prefab 
    public GameObject Prefab;
    // public float that will adjust the speed of the object sent out
    public float FireSpeed = 5;


    // Update is called once per frame
    void Update()
    {
        // if left mouse is held down (Fire1 can be changed but stands as right mouse button)
        if ( Input.GetButton( "Fire1" ) )
        {
            //find the point in which the click has happened 
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            // find the direction the prefab should move in
            Vector3 FireDirection = clickPoint - this.transform.position;
            // normalize that direction
            FireDirection.Normalize();
            // create and instance of the prefab at the location of this script
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            // send the prefab in the fireDirection at a certain speed.
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }

}
