﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public float deathClock = 5;

    // Update is called once per frame
    void Update()
    {
        deathClock -= Time.deltaTime;

        if (deathClock < 0) {
            Destroy(gameObject);
        }
    }
}
