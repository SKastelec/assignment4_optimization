﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    Renderer objRenderer;

    void Awake() {
        objRenderer = GetComponent<Renderer>();
    }

    // Use this for initialization
    void Start()
    {
        // when this object starts, randomly choose a colour for this object
        objRenderer.material.color = new Color( Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), Random.Range( 0f, 1f ) );
    }

}
