﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    // reference to the collectable gameobject
    public GameObject Collectible;

    // reference to the current collectable in game
    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        // if there is no current collectable in the game
        if ( m_currentCollectible == null )
        {
            //create a new collectable at one of the random spawn points with the collectables rotaion
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
        }
    }
}
