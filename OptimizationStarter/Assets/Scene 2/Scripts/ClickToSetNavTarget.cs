﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        // update camera variable to find the main camera at all times
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        // update to always be getting new info on the raycast
        RaycastHit hit = new RaycastHit();

        //constantly update the raycast to always be tracking if the mouse is hitting the "Ground" and move to the move if it's true
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
