﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    //public float radius that will define the radius of the overlapSphere
    public float Radius;

    public void Update()
    {
        //always update this array of colliders to detect for intersecting colliders
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );

        // for every collider in the colliders array
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            // if one of those colliders has the tag of player
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                //destroy this gameobject
                Destroy( this.gameObject );
            }
        }
    }

}
